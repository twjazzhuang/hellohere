//
//  ViewController.swift
//  HelloHere
//
//  Created by Jazz Huang on 2019/11/29.
//  Copyright © 2019 Jazz Huang. All rights reserved.
//

import UIKit
import NMAKit

class ViewController: UIViewController,NMAMapViewDelegate,NMAMapGestureDelegate {
    @IBOutlet weak var mapView: NMAMapView!
    @IBOutlet weak var dialogView: UIStackView!
    @IBOutlet weak var addressInfoview: UITextView!
    @IBOutlet weak var setRouteBtn: UIButton!
    @IBOutlet weak var requestControl: UISegmentedControl!
    
    var requestCompletion: NMARequestCompletionBlock!
    var mapObjectsArray = [NMAMapObject]()
    var resultsArray: [NMALink] = []
    var positionManager:NMAPositioningManager!
    var placeLink: NMAPlaceLink? = nil
    var selectedPosition: NMAGeoCoordinates? = nil
    var selectedMarker: NMAMapMarker? = nil
    var coreRouter: NMACoreRouter!
    var mapRouts = [NMAMapRoute]()
    var route : [NMAGeoCoordinates] = []
    var centerPoints : [NMAMapMarker] = []
    var centerPosition: NMAGeoCoordinates? = nil
    var currentPosition: NMAGeoCoordinates? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initValue()

    }
    
    func initView(){
        //設定幾個 ui 元件隱藏
        requestControl.selectedSegmentIndex = UISegmentedControl.noSegment
        requestControl.isHidden = true
        setRouteBtn.isHidden = true
        findCurrentPosition()
    }
    func initValue(){
        //簡單設置一些元件
        coreRouter = NMACoreRouter()
        mapView.gestureDelegate = self
        mapView.delegate = self
        initCompletion()
    }
    
    //初始化 request Completion
    func initCompletion(){
        //搜尋附近景點的 request 處理
        self.requestCompletion = {[weak self] request, data, inError in
            guard inError == nil else {
                print ("discovery request error \((inError! as NSError).code)")
                return
            }
            guard data is NMADiscoveryPage, let resultPage = data as? NMADiscoveryPage else {
                print ("invalid type returned \(String(describing: data))")
                return
            }
            if let strongSelf = self {
                strongSelf.resultsArray = resultPage.discoveryResults;
                for link in strongSelf.resultsArray
                {
                    //景點放大頭針
                    if let placeLink = link as? NMAPlaceLink {
                        strongSelf.addMarkerAtPlace(placeLink);
                    }
                }
            }
        }
    }
    
    //路線指引元件
    @IBAction func handleRequestControl(_ sender: UISegmentedControl) {
        self.clearRoutes() //清除舊的路線
        let routingMode = NMARoutingMode.init(
            routingType: NMARoutingType.fastest,
            transportMode: NMATransportMode.car,
            routingOptions: NMARoutingOption.avoidHighway
        )
        switch self.requestControl.selectedSegmentIndex {
            case 0:
                routingMode.transportMode = .car
            case 1:
                routingMode.transportMode = .publicTransport
            case 2:
                routingMode.transportMode = .pedestrian
            case 3:
                routingMode.transportMode = .bike
            default:
                print("Trigger not implemented")
        }
        //路線請求
        self.routeRequest(routingMode: routingMode)
    }

    //規劃路線
    @IBAction func setRouteBtn(_ sender: UIButton) {
        self.mapView.positionIndicator.isVisible = true
        //設定起點跟終點 起點是使用者位置 終點是 選擇的marker地點
        route.append(currentPosition!)
        route.append(selectedPosition!)
        //清除所有的 marker 只顯示前往的 marker
        if !self.mapObjectsArray.isEmpty{
            self.mapView.remove(mapObjects:self.mapObjectsArray)
            self.mapObjectsArray.removeAll()
        }
        self.mapObjectsArray.append(selectedMarker!)
        self.mapView.add(mapObject: selectedMarker!)
        //預設的路線是 car
        let routingMode = NMARoutingMode.init(
            routingType: NMARoutingType.fastest,
            transportMode: NMATransportMode.car,
            routingOptions: NMARoutingOption.avoidHighway
        )
        self.routeRequest(routingMode: routingMode)
        //顯示路線指引元件
        requestControl.isHidden = false
    }
    
    
    //新增標記
    func addMarkerAtPlace(_ placeLink: NMAPlaceLink) -> Void {
        if let uiImage = UIImage(named:"markerIcon.png") {
            let mapMarker = NMAMapMarker(geoCoordinates: placeLink.position ?? self.mapView.geoCenter, image: uiImage)
            self.mapView.add(mapObject: mapMarker)
            self.mapObjectsArray.append(mapMarker)
        }
    }
    
    //初始定位使用者位置
    private func findCurrentPosition() {
        let positioningManager = NMAPositioningManager.sharedInstance()
        guard positioningManager.startPositioning() else {
            print("Error: Positioning failed to start.")
            return
        }

        // 訂閱使用者位置
        var token: NSObjectProtocol?
        token = NotificationCenter.default.addObserver(
            forName: .NMAPositioningManagerDidUpdatePosition,
            object: positioningManager,
            queue: OperationQueue.main) { _ in

                guard
                    let position = NMAPositioningManager.sharedInstance().currentPosition,
                    position.isValid,
                    let coordinates = position.coordinates else {
                        // Opt out until we get valid coordinates.
                        print("No position found.")
                        return
                }
                // Unsubscribe position updates. We want to start guidance only once.
                token.flatMap(NotificationCenter.default.removeObserver)
                
                //設定使用者定位在畫面中間 set current position in vivw center.
                self.mapView.set(geoCenter: coordinates, animation:NMAMapAnimation.none)
                print("中間在這")
                self.currentPosition = coordinates
                //繪製一個 初始的畫面中間 ｍａｒｋｅｒ
                guard let image = UIImage(named: "center") else { return }
                let indicatorMarker = NMAMapMarker(geoCoordinates: self.mapView.geoCenter, image: image)
                self.mapView.add(mapObject: indicatorMarker)
                //放入中間 center point
                self.centerPoints.append(indicatorMarker)
                NMAGeocoder.sharedInstance().createReverseGeocodeRequest(coordinates: self.mapView.geoCenter).start(
                   self.parseResultFromReverseGeocodeRequest(request:requestData:error:)
                )
        }
    }
    
    //把 經緯度 轉換成地址
    func parseResultFromReverseGeocodeRequest(request: NMARequest?, requestData data: Any?, error: Error?) {
        if error != nil {
            print("error \(error!.localizedDescription)")
            return
        }
        
        if (!(request is NMAReverseGeocodeRequest)) {
            print("invalid type returned")
            return
        }
        
        guard let arr = data as? NSArray, arr.count != 0 else {
            return
        }
        
        if let address = (arr.object(at: 0) as? NMAReverseGeocodeResult)?.location?.address?.formattedAddress {
            print("地址：")
            addressInfoview.text = "Address:" + address
            print(address)
        }
    }
    
    
    
    //點擊地圖物件
    func mapView(_ mapView: NMAMapView, didSelect objects: [NMAViewObject]) {
        print("map view delegate method called")
        selectedPosition = nil
        for object in objects {
            selectedMarker = object as? NMAMapMarker
            for place in resultsArray {
                if let placeLink = place as? NMAPlaceLink {
                    let objLocation = object.location()!
                    // 篩選 點擊的物件是什麼
                    if objLocation.isEqual(placeLink.position!) {
                        selectedPosition = placeLink.position!
                        addressInfoview.text = "Name:\(placeLink.name!)\nAddress:\(placeLink.vicinityDescription!)"
                    }
                }
            }
        }
        setRouteBtn.isHidden = false
    }
    
    //畫面開始移動
    func mapViewDidBeginMovement(_ mapView: NMAMapView) {
        self.cleanMap()
        self.clearRoutes()
        requestControl.selectedSegmentIndex = UISegmentedControl.noSegment
        requestControl.isHidden = true
        setRouteBtn.isHidden = true
        self.centerPoints.removeAll()
        self.route.removeAll()
    }
    
    //畫面停止移動的時候
    func mapViewDidEndMovement(_ mapView: NMAMapView) {
        self.cleanMap()
        //繪製一個ｍａｒｋｅｒ
        guard let image = UIImage(named: "center") else { return }
        let indicatorMarker = NMAMapMarker(geoCoordinates: self.mapView.geoCenter, image: image)
        centerPosition = self.mapView.geoCenter
        self.mapView.add(mapObject: indicatorMarker)
        self.centerPoints.append(indicatorMarker)
        
        print("pois here")
        let exploreRequest = NMAPlaces.sharedInstance()?.createExploreRequest(location: self.mapView.geoCenter, searchArea: self.mapView.boundingBox, filters: nil)
        
        NMAGeocoder.sharedInstance().createReverseGeocodeRequest(coordinates: self.mapView.geoCenter).start(
           self.parseResultFromReverseGeocodeRequest(request:requestData:error:)
        )
        
        if (exploreRequest?.start(self.requestCompletion) as NSError?) != nil {
            print("error")
        }
    }
    
    func cleanMap () {
        if !self.mapObjectsArray.isEmpty{
            self.mapView.remove(mapObjects:self.mapObjectsArray)
            self.mapObjectsArray.removeAll()
        }
        if !self.resultsArray.isEmpty{
            self.resultsArray.removeAll()
        }
        if !self.centerPoints.isEmpty {
            self.mapView.remove(mapObjects:self.centerPoints)
            self.centerPoints.removeAll()
        }
    }
    
    //路線請求
    func routeRequest(routingMode:NMARoutingMode){
        
        coreRouter.calculateRoute(withStops: route, routingMode: routingMode, { (routeResult, error) in
                if (error != NMARoutingError.none) {
                    NSLog("Error in callback: \(error)")
                    return
                }
                
                guard let route = routeResult?.routes?.first else {
                    print("Empty Route result")
                    return
                }
                
                guard let box = route.boundingBox, let mapRoute = NMAMapRoute.init(route) else {
                    print("Can't init Map Route")
                    return
                }
                
                if (self.mapRouts.count != 0) {
                    for route in self.mapRouts {
                        self.mapView.remove(mapObject: route)
                    }
                    self.mapRouts.removeAll()
                }
                
                self.mapRouts.append(mapRoute)
        
                self.mapView.set(boundingBox: box, animation: NMAMapAnimation.linear)
                self.mapView.add(mapObject: mapRoute)
            })
    }
    
    //路線清除
    func clearRoutes() {
        for route in mapRouts {
            mapView.remove(mapObject: route)
        }
    }
    
}

