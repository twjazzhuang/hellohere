//
//  AppDelegate.swift
//  HelloHere
//
//  Created by Jazz Huang on 2019/11/29.
//  Copyright © 2019 Jazz Huang. All rights reserved.
//

import UIKit
import NMAKit

let credentials = (
    appId: "id",
    appCode: "code",
    licenseKey: "key"
)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        NMAApplicationContext.setAppId(credentials.appId, appCode: credentials.appCode, licenseKey: credentials.licenseKey)
        return true
    }
}
